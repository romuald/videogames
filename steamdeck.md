# Matériels
## Dock/station d'accueil
Pour jouer sur un écran externe, il faut à minima un adaptateur USB 3 vers une sortie vidéo.

Cependant avoir un dock avec une entrée "Power Delivery" en supplément de la sortie vidéo, permet de le charger et de poser le Steam Deck dans une position où il peut correctement ventiller.

Il existe une grande diversité de dock, notamment pour la variété de port de sortie et différents supports (par ex: SSD).

### Exemple de dock avec sortie HDMI de la marque jsaux
- _5 en 1_ à 40e (26e solde) : hdmi/usb power delivery 100w/2 usb 2.0/ethernet 100 Mbps
- _6 en 1_ à 50e (37e solde) : hdmi/usb power delivery 100w/3 usb 3.0/ethernet 1 Gbps

## xbox controller
Si la connexion bluetooth n'est pas stable ou si plusieurs manettes sont reconnues comme la même, mettre à jour le firmware des manettes.
Sous Windows via l'application "Accessoires xbox" qui peut vous demandez d'installer l'application xbox qui n'est pas nécessaire.

# Logiciels
## Jeux sur Steam Deck
Les jeux non supportés par Valve peuvent tout de même fonctionner:

- soit directement
- soit en indiquant une version de Proton embarqué dans Steam
- soit via les couches de compatibilité de la communauté, notamment ProtonGE, voir https://www.protondb.com

### Les sauvegardes
Pour les jeux hors Steam ou n'ayant pas la fonctionnalité Steam Cloud, il est nécessaire de récupérer/restaurer les sauvegardes.

Les emplacements diffèrent selon les jeux, une source d'information est https://www.pcgamingwiki.com/wiki/Glossary:Game_data

Et pour un jeu spécifique https://www.pcgamingwiki.com/wiki/${game}

#### Proton/Windows
Les jeux tournant sous Proton stockeront probablement leurs sauvegardes dans des sous-répertoires de la couche de compatibilité compatdata.

Par exemple, Omno (dont le steamid est 969760) utilise le standard AppData de Windows:

- **/home/deck/.local/share/Steam/steamapps/compatdata/**_969760/pfx_**/drive_c/users/steamuser/AppData/**_Local/Omno/Saved/SaveGames/_

#### Unity
Les jeux utilisant le moteur Unity peuvent avoir leurs sauvegardes dans **/home/deck/.config/unity3d/**, voir https://docs.unity3d.com/ScriptReference/PlayerPrefs.html

#### Linux
Les emplacements standards sous Linux sont:

- **$XDG_DATA_HOME**
- **/home/deck/.local/share/**
- **/home/deck/.config**
- **/home/deck/.${game}**

## Steam
### Personnalisation
#### Steam Input
Steam Input permet beaucoup de réglages différents dont la gestion du gyroscope ou encore l'association d'actions multiples à une même commande.

Par exemple, dans les jeux de tir une gachette peut permettre d'activer le gyroscope pour la visée puis la relacher repositionner le viseur au centre.

#### Les vidéos d'accueil
Des vidéos sont regroupées sur https://steamdeckrepo.com/

Elles sont à copier dans **/home/deck/.local/share/Steam/config/uioverrides/**

#### Les images des jeux
Des images sont regroupées sur https://www.steamgriddb.com/

##### Via Steam
En bureau: _Bibliothèque > Accueil > Jeux récents_

- icône via _Clic droit > Propriétés... > Clic gauche sur l'emplacement précédent le nom_
- image horizontale d'arrière-plan après _sélection du jeu souhaité via Clic droit sur l'emplacement de la bannière > "Définir un arrière-plan personnalisé"_
- le logo allant sur la bannière après _sélection du jeu souhaité via Clic droit sur l'emplacement de la bannière > "Définir un logo personnalisé"_
- l'image verticale de grille peut être définie à partir des jeux récents en dehors du dernier ou à partir de _"Tous les jeux", Clic droit > Gérer > "Définir une image personnalisée"_
- l'image horizontale de grille ne peut être définie que lorsque le jeu souhaité est _le dernier jeu lancé > Clic droit > Gérer > "Définir une image personnalisée"_

##### Par copie de fichiers
Dans **/home/deck/.local/share/Steam/userdata/${account_id}/config/grid/**

${account_id} peut-être trouvé

- soit car c'est le seul répertoire dans userdata si vous n'avez qu'un compte connecté
- soit via https://steamdb.info/calculator/${steamid}. Le steamid est dans "Gérer mon compte" > "Détail du compte" (en haut à gauche, à côté de "Ouvrir le mode Big Picture")
- soit en applicant le calcul de https://github.com/xPaw/SteamID.php/blob/master/SteamID.php

- image horizontale d'arrière-plan ${id_game}_hero.ext
- le logo ${id_game}_logo.ext
- l'image verticale de grille ${id_game}p.ext
- l'image horizontale de grille ${id_game}.ext

Les icônes ne sont pas copiées ailleurs, Steam stocke les informations sur les raccourcis dans **/home/deck/.local/share/Steam/userdata/${account_id}/config/shortcuts.vdf**

Par ailleurs pour les jeux Steam des icônes sont mises en cache dans **/home/deck/.local/share/Steam/steam/games/** et d'autres avec les images dans **/home/deck/.local/share/Steam/appcache/librarycache/**

### Bandes son
Elles se retrouvent dans **/home/deck/.local/share/Steam/steamapps/music/**

## Non steam
Les logiciels et jeux installés via le mode Bureau sont ajoutables à Steam à partir d'un clic-droit sur le fichier exécutable ou le fichier desktop associé.

### Steam Input
Certains logiciels/jeux gèrent directement les contrôleurs, pour ces derniers Steam Input peut poser soucis. Dans ce cas sous Steam, sur la page du logiciel/jeu, _Paramètres du contrôleur/l'icône manette > l'icône de roue crantée > Désactiver Steam Input_.

Les contrôleurs peuvent ne pas être détectés de la même manière entre l'interface "game mode" et "desktop mode". De plus en "game mode" ils servent à controler l'interface c'est pourquoi il peut-être préférable de les configurer en "desktop mode".

### Installations ciblant Windows
Si l'application est prévue pour Windows il faudra la lancer avec une couche de compatibilité à définir dans _Gérer > Propriétés... > Compatibilité_

Un répertoire sera alors créé dans **/home/deck/.local/share/Steam/steamapps/compatdata/** il est associé au jeu sous Steam, supprimer le jeu de Steam supprime le répertoire.

### Installations à base de flatpack
#### Modification des permissions
- _Configuration du système > Applications > Configuration des permissions pour Flatpak_
- flatseal
- **/home/deck/.local/share/flatpak/overrides/**

Notamment pour ajouter des répertoires accessibles depuis l'application installée via flatpack ou encore retirer l'accès à internet pour augmenter la sécurité.

#### Création de .desktop
Le format desktop permet notamment d'ajouter des paramètres et options à la commande de lancement de l'exécutable.
Afin d'avoir l'application dans le menu mettre le fichier desktop dans **/home/deck/.local/share/applications/**
Les icônes peuvent être mis dans **/home/deck/.local/share/icons/**

##### Cas d'une webapp
${url_webapp} est à remplacer par l'URL du site souhaité.

Avec Firefox
```sh
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Webapp
Exec=/usr/bin/flatpak run --branch=stable --arch=x86_64 --file-forwarding --command=firefox org.mozilla.firefox --kiosk @@u ${url_webapp} @@
Icon=/home/deck/Pictures/apps/webapp.png
Terminal=false
Type=Application
Categories=Network;
```

Avec Brave, seul la commande diffère
```sh
Exec=/usr/bin/flatpak run --branch=stable --arch=x86_64 --file-forwarding --command=brave com.brave.Browser "–-force-device-scale-factor=1.25" "–-device-scale-factor=1.25" "--kiosk" @@u ${url_webapp} @@
```
